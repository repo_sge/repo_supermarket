﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using SIGEFA.Entidades;

namespace SIGEFA.Interfaces
{
    interface IRepositorio
    {
        Boolean registra_repositorio(clsRepositorio repo);
        List<clsRepositorio> buscar_repositorio(clsRepositorio repo);
        List<clsRepositorio> listar_repositorio(String estado, Int32 codsuc, Int32 codalma, DateTime fecha);
        Boolean actualiza_repositorio(clsRepositorio repo);

        Boolean ActualizaCorrelativoDocResp(Int32 codtipodoc, Int32 codalma);
        List<clsRepositorio> listar_repositorio_Enviados(String estado, Int32 codsucu, Int32 codalma, DateTime fecha);

        //para resumen diario
        int ultimoCorrelativoResumen(DateTime fecha);
        DataTable listaBoletasResumen(DateTime fecha,int codalma);

        Boolean registra_documento_electronico(clsDocumentoElectronico doc);
        Boolean registra_ResumenDiario(clsResumenDiario doc, DataTable detalle);

        DataTable listaResumenes(int codalma, DateTime desde, DateTime hasta);

        clsResumenDiario getResumenxId(int codresumen);
        DataTable listaBoletasResumenxId(int codresumen);
        Boolean updateResumenxId(clsResumenDiario doc);

        DataTable listaTicketsxEstado(DateTime desde, DateTime hasta, string estado);
        //Boolean registra_detalleResumen(DataTable detalle);
        Boolean updateRepositorioBoletas(clsResumenDiario doc);
    }
}
