﻿using SIGEFA.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIGEFA.Interfaces
{
	interface INotaIngresoIgv
	{
		Boolean insert(clsNotaIngresoIgv notaIngresoIgv);
	}
}
