﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using SIGEFA.Entidades;
using SIGEFA.Interfaces;
using SIGEFA.InterMySql;

namespace SIGEFA.Administradores
{
    class clsAdmRepositorio
    {
        IRepositorio irepo = new MysqlRepositorio();

        public bool registra_repositorio(Entidades.clsRepositorio repo)
        {
            return irepo.registra_repositorio(repo);            
        }

        public List<Entidades.clsRepositorio> buscar_repositorio(Entidades.clsRepositorio repo) {

            return irepo.buscar_repositorio(repo);        
        }
        
        public List<Entidades.clsRepositorio> listar_repositorio(String estado, Int32 codsucu, Int32 codalma, DateTime fecha)
        {
            return irepo.listar_repositorio(estado, codsucu, codalma, fecha);
        }

        public bool actualiza_repositorio(Entidades.clsRepositorio repo) {

            return irepo.actualiza_repositorio(repo);
        }

        public bool ActualizaCorrelativoDocResp(Int32 codtipodoc, Int32 codalma)
        {
            return irepo.ActualizaCorrelativoDocResp(codtipodoc, codalma);
        }

        public List<Entidades.clsRepositorio> listar_repositorio_Enviados(String estado, Int32 codsucu, Int32 codalma, DateTime fecha)
        {
            return irepo.listar_repositorio_Enviados(estado, codsucu, codalma, fecha);
        }
        
        //resumen diario de boletas
        public int ultimoCorrelativoResumen(DateTime fecha)
        {
            return irepo.ultimoCorrelativoResumen(fecha);
        }

        public DataTable listaBoletasResumen(DateTime fecha, Int32 codalma)
        {
            return irepo.listaBoletasResumen(fecha,codalma);
        }

        public bool registra_documento_electronico(clsDocumentoElectronico doc)
        {
            return irepo.registra_documento_electronico(doc);
        }

        public bool registra_ResumenDiario(clsResumenDiario doc,DataTable detalle)
        {
            return irepo.registra_ResumenDiario(doc,detalle);
        }

        public DataTable listaResumenes(int codalma,DateTime desde, DateTime hasta)
        {
            return irepo.listaResumenes(codalma,desde,hasta);
        }

        public clsResumenDiario getResumenxId(int codresumen)
        {
            return irepo.getResumenxId(codresumen);
        }

        public DataTable listaBoletasResumenxId(int codresumen)
        {
            return irepo.listaBoletasResumenxId(codresumen);
        }

        public bool updateResumenxId(clsResumenDiario doc)
        {
            return irepo.updateResumenxId(doc);
        }

        public DataTable listaTicketsxEstado(DateTime desde, DateTime hasta, string estado)
        {
            return irepo.listaTicketsxEstado(desde,hasta,estado);
        }

        public bool updateRepositorioBoletas(clsResumenDiario doc)
        {
            return irepo.updateRepositorioBoletas(doc);
        }
    }
}
