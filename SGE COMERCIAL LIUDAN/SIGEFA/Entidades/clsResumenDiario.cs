﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGEFA.Entidades
{
    class clsResumenDiario
    {

        public int Resumenid { get; set; }

        public string Tipodoc { get; set; }

        public DateTime Fechaemision { get; set; }

        public DateTime Fechadocumentos { get; set; }

        public DateTime Fecharegistro { get; set; }

        public DateTime Fechaenvio { get; set; }

        public int CantidadDocs { get; set; }

        public string Correlativo { get; set; }

        public decimal Monto { get; set; }

        public string Estadosunat { get; set; }

        public string Mensajesunat { get; set; }

        public string Hash { get; set; }

        public string Nombredoc { get; set; }

        public int Usuario { get; set; }

        public int CodEmpresa { get; set; }

        public int CodSucursal { get; set; }

        public int CodAlmacen { get; set; }

        public string Ticket { get; set; }
    }
}
