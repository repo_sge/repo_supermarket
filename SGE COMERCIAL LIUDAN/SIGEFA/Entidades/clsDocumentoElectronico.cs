﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGEFA.Entidades
{
    class clsDocumentoElectronico
    {

        public int Repoid { get; set; }

        public string Tipodoc { get; set; }

        public DateTime Fechaemision { get; set; }

        public string Serie { get; set; }

        public string Correlativo { get; set; }

        public decimal Monto { get; set; }

        public string Estadosunat { get; set; }

        public string Mensajesunat { get; set; }

        public string Hash { get; set; }

        public string Nombredoc { get; set; }

        public int Usuario { get; set; }

        public int CodEmpresa { get; set; }

        public int CodSucursal { get; set; }

        public int CodAlmacen { get; set; }

        public int CodFacturaVenta { get; set; }

    }
}
