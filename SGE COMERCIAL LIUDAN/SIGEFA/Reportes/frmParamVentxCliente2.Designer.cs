﻿namespace SIGEFA.Reportes
{
    partial class frmParamVentxCliente2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label8 = new System.Windows.Forms.Label();
			this.dtpFecha2 = new System.Windows.Forms.DateTimePicker();
			this.dtpFecha1 = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.txtCodCli = new System.Windows.Forms.TextBox();
			this.txtCliente = new System.Windows.Forms.TextBox();
			this.txtUnCli = new System.Windows.Forms.TextBox();
			this.rbCli = new System.Windows.Forms.RadioButton();
			this.rbTodosCli = new System.Windows.Forms.RadioButton();
			this.btnCancelar = new System.Windows.Forms.Button();
			this.btnReporte = new System.Windows.Forms.Button();
			this.rbTodosArt = new System.Windows.Forms.RadioButton();
			this.rbArt = new System.Windows.Forms.RadioButton();
			this.txtUnArt = new System.Windows.Forms.TextBox();
			this.txtArticulo = new System.Windows.Forms.TextBox();
			this.txtCodProd = new System.Windows.Forms.TextBox();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label8);
			this.groupBox1.Controls.Add(this.dtpFecha2);
			this.groupBox1.Controls.Add(this.dtpFecha1);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(16, 15);
			this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
			this.groupBox1.Size = new System.Drawing.Size(643, 84);
			this.groupBox1.TabIndex = 9;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "FILTRO DE FECHAS";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.BackColor = System.Drawing.Color.Transparent;
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.ForeColor = System.Drawing.Color.SteelBlue;
			this.label8.Location = new System.Drawing.Point(313, 20);
			this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(49, 16);
			this.label8.TabIndex = 45;
			this.label8.Text = "Hasta";
			// 
			// dtpFecha2
			// 
			this.dtpFecha2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpFecha2.Location = new System.Drawing.Point(316, 38);
			this.dtpFecha2.Margin = new System.Windows.Forms.Padding(4);
			this.dtpFecha2.Name = "dtpFecha2";
			this.dtpFecha2.Size = new System.Drawing.Size(131, 22);
			this.dtpFecha2.TabIndex = 28;
			// 
			// dtpFecha1
			// 
			this.dtpFecha1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpFecha1.Location = new System.Drawing.Point(25, 40);
			this.dtpFecha1.Margin = new System.Windows.Forms.Padding(4);
			this.dtpFecha1.Name = "dtpFecha1";
			this.dtpFecha1.Size = new System.Drawing.Size(131, 22);
			this.dtpFecha1.TabIndex = 38;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.BackColor = System.Drawing.Color.Transparent;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.SteelBlue;
			this.label1.Location = new System.Drawing.Point(23, 20);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(54, 16);
			this.label1.TabIndex = 26;
			this.label1.Text = "Desde";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.txtCodCli);
			this.groupBox2.Controls.Add(this.txtCliente);
			this.groupBox2.Controls.Add(this.txtUnCli);
			this.groupBox2.Controls.Add(this.rbCli);
			this.groupBox2.Controls.Add(this.rbTodosCli);
			this.groupBox2.Location = new System.Drawing.Point(16, 107);
			this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
			this.groupBox2.Size = new System.Drawing.Size(643, 82);
			this.groupBox2.TabIndex = 11;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "CLIENTE";
			// 
			// txtCodCli
			// 
			this.txtCodCli.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtCodCli.Enabled = false;
			this.txtCodCli.Location = new System.Drawing.Point(595, 48);
			this.txtCodCli.Margin = new System.Windows.Forms.Padding(4);
			this.txtCodCli.Name = "txtCodCli";
			this.txtCodCli.Size = new System.Drawing.Size(39, 22);
			this.txtCodCli.TabIndex = 69;
			this.txtCodCli.Visible = false;
			// 
			// txtCliente
			// 
			this.txtCliente.Enabled = false;
			this.txtCliente.Location = new System.Drawing.Point(259, 48);
			this.txtCliente.Margin = new System.Windows.Forms.Padding(4);
			this.txtCliente.Name = "txtCliente";
			this.txtCliente.Size = new System.Drawing.Size(329, 22);
			this.txtCliente.TabIndex = 62;
			// 
			// txtUnCli
			// 
			this.txtUnCli.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtUnCli.Enabled = false;
			this.txtUnCli.Location = new System.Drawing.Point(139, 48);
			this.txtUnCli.Margin = new System.Windows.Forms.Padding(4);
			this.txtUnCli.Name = "txtUnCli";
			this.txtUnCli.Size = new System.Drawing.Size(111, 22);
			this.txtUnCli.TabIndex = 61;
			this.txtUnCli.TextChanged += new System.EventHandler(this.txtUnCli_TextChanged);
			this.txtUnCli.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtUnCli_KeyDown);
			this.txtUnCli.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUnCli_KeyPress);
			// 
			// rbCli
			// 
			this.rbCli.AutoSize = true;
			this.rbCli.BackColor = System.Drawing.Color.Transparent;
			this.rbCli.Location = new System.Drawing.Point(25, 52);
			this.rbCli.Margin = new System.Windows.Forms.Padding(4);
			this.rbCli.Name = "rbCli";
			this.rbCli.Size = new System.Drawing.Size(87, 20);
			this.rbCli.TabIndex = 57;
			this.rbCli.Text = "Un Cliente";
			this.rbCli.UseVisualStyleBackColor = false;
			this.rbCli.CheckedChanged += new System.EventHandler(this.rbCli_CheckedChanged);
			// 
			// rbTodosCli
			// 
			this.rbTodosCli.AutoSize = true;
			this.rbTodosCli.BackColor = System.Drawing.Color.Transparent;
			this.rbTodosCli.Checked = true;
			this.rbTodosCli.Location = new System.Drawing.Point(25, 23);
			this.rbTodosCli.Margin = new System.Windows.Forms.Padding(4);
			this.rbTodosCli.Name = "rbTodosCli";
			this.rbTodosCli.Size = new System.Drawing.Size(136, 20);
			this.rbTodosCli.TabIndex = 54;
			this.rbTodosCli.TabStop = true;
			this.rbTodosCli.Text = "Todos los clientes";
			this.rbTodosCli.UseVisualStyleBackColor = false;
			// 
			// btnCancelar
			// 
			this.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnCancelar.ImageIndex = 0;
			this.btnCancelar.Location = new System.Drawing.Point(559, 287);
			this.btnCancelar.Margin = new System.Windows.Forms.Padding(4);
			this.btnCancelar.Name = "btnCancelar";
			this.btnCancelar.Size = new System.Drawing.Size(100, 28);
			this.btnCancelar.TabIndex = 14;
			this.btnCancelar.Text = "Cancelar";
			this.btnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.btnCancelar.UseVisualStyleBackColor = true;
			this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
			// 
			// btnReporte
			// 
			this.btnReporte.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnReporte.ImageIndex = 1;
			this.btnReporte.Location = new System.Drawing.Point(451, 287);
			this.btnReporte.Margin = new System.Windows.Forms.Padding(4);
			this.btnReporte.Name = "btnReporte";
			this.btnReporte.Size = new System.Drawing.Size(100, 28);
			this.btnReporte.TabIndex = 13;
			this.btnReporte.Text = "Reporte";
			this.btnReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.btnReporte.UseVisualStyleBackColor = true;
			this.btnReporte.Click += new System.EventHandler(this.btnReporte_Click);
			// 
			// rbTodosArt
			// 
			this.rbTodosArt.AutoSize = true;
			this.rbTodosArt.BackColor = System.Drawing.Color.Transparent;
			this.rbTodosArt.Checked = true;
			this.rbTodosArt.Location = new System.Drawing.Point(25, 23);
			this.rbTodosArt.Margin = new System.Windows.Forms.Padding(4);
			this.rbTodosArt.Name = "rbTodosArt";
			this.rbTodosArt.Size = new System.Drawing.Size(140, 20);
			this.rbTodosArt.TabIndex = 54;
			this.rbTodosArt.TabStop = true;
			this.rbTodosArt.Text = "Todos los artículos";
			this.rbTodosArt.UseVisualStyleBackColor = false;
			// 
			// rbArt
			// 
			this.rbArt.AutoSize = true;
			this.rbArt.BackColor = System.Drawing.Color.Transparent;
			this.rbArt.Location = new System.Drawing.Point(25, 52);
			this.rbArt.Margin = new System.Windows.Forms.Padding(4);
			this.rbArt.Name = "rbArt";
			this.rbArt.Size = new System.Drawing.Size(90, 20);
			this.rbArt.TabIndex = 57;
			this.rbArt.Text = "Un Artículo";
			this.rbArt.UseVisualStyleBackColor = false;
			this.rbArt.CheckedChanged += new System.EventHandler(this.rbArt_CheckedChanged);
			// 
			// txtUnArt
			// 
			this.txtUnArt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtUnArt.Enabled = false;
			this.txtUnArt.Location = new System.Drawing.Point(139, 48);
			this.txtUnArt.Margin = new System.Windows.Forms.Padding(4);
			this.txtUnArt.Name = "txtUnArt";
			this.txtUnArt.Size = new System.Drawing.Size(111, 22);
			this.txtUnArt.TabIndex = 61;
			this.txtUnArt.TextChanged += new System.EventHandler(this.txtUnArt_TextChanged);
			this.txtUnArt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtUnArt_KeyDown);
			this.txtUnArt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUnArt_KeyPress);
			// 
			// txtArticulo
			// 
			this.txtArticulo.Enabled = false;
			this.txtArticulo.Location = new System.Drawing.Point(259, 48);
			this.txtArticulo.Margin = new System.Windows.Forms.Padding(4);
			this.txtArticulo.Name = "txtArticulo";
			this.txtArticulo.Size = new System.Drawing.Size(329, 22);
			this.txtArticulo.TabIndex = 63;
			// 
			// txtCodProd
			// 
			this.txtCodProd.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtCodProd.Enabled = false;
			this.txtCodProd.Location = new System.Drawing.Point(595, 48);
			this.txtCodProd.Margin = new System.Windows.Forms.Padding(4);
			this.txtCodProd.Name = "txtCodProd";
			this.txtCodProd.Size = new System.Drawing.Size(39, 22);
			this.txtCodProd.TabIndex = 69;
			this.txtCodProd.Visible = false;
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.txtCodProd);
			this.groupBox4.Controls.Add(this.txtArticulo);
			this.groupBox4.Controls.Add(this.txtUnArt);
			this.groupBox4.Controls.Add(this.rbArt);
			this.groupBox4.Controls.Add(this.rbTodosArt);
			this.groupBox4.Location = new System.Drawing.Point(16, 197);
			this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
			this.groupBox4.Size = new System.Drawing.Size(643, 82);
			this.groupBox4.TabIndex = 62;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "ARTÍCULO";
			// 
			// frmParamVentxCliente2
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
			this.ClientSize = new System.Drawing.Size(675, 331);
			this.Controls.Add(this.groupBox4);
			this.Controls.Add(this.btnCancelar);
			this.Controls.Add(this.btnReporte);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.DoubleBuffered = true;
			this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Margin = new System.Windows.Forms.Padding(4);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmParamVentxCliente2";
			this.ShowIcon = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Reporte de Ventas por Cliente / Articulo";
			this.Load += new System.EventHandler(this.frmParamVentxCliente2_Load_1);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox4.ResumeLayout(false);
			this.groupBox4.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dtpFecha1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DateTimePicker dtpFecha2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnReporte;
        private System.Windows.Forms.RadioButton rbTodosCli;
        private System.Windows.Forms.RadioButton rbCli;
        public System.Windows.Forms.TextBox txtUnCli;
        public System.Windows.Forms.TextBox txtCliente;
        public System.Windows.Forms.TextBox txtCodCli;
        private System.Windows.Forms.RadioButton rbTodosArt;
        private System.Windows.Forms.RadioButton rbArt;
        public System.Windows.Forms.TextBox txtUnArt;
        public System.Windows.Forms.TextBox txtArticulo;
        public System.Windows.Forms.TextBox txtCodProd;
        private System.Windows.Forms.GroupBox groupBox4;
    }
}