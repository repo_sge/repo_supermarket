﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SIGEFA.Interfaces;
using SIGEFA.Conexion;
using MySql.Data.MySqlClient;
using System.Data;
using SIGEFA.Entidades;
using System.Transactions;

namespace SIGEFA.InterMySql
{
    class MysqlRepositorio:IRepositorio
    {
        private clsConexionMysql con = new clsConexionMysql();
        private MySqlCommand cmd = null;
        private MySqlDataReader dr = null;
        private MySqlDataAdapter adap = null;
        private MySqlTransaction mysqltransaccion;
        private List<clsRepositorio> lista = null;
        private clsRepositorio clsrepo = null;
        private string consulta = "";
        private DataTable tabla;

        public bool registra_repositorio(Entidades.clsRepositorio repo)
        {
            try
            {
                con.conectarBD();

                mysqltransaccion = con.conector.BeginTransaction();
                consulta = "registrar_repositorio";
                cmd = new MySqlCommand(consulta, con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("_tipdoc", repo.Tipodoc);
                oParam = cmd.Parameters.AddWithValue("_fechaemision", repo.Fechaemision.ToString("yyyy/MM/dd"));
                oParam = cmd.Parameters.AddWithValue("_serie", repo.Serie);
                oParam = cmd.Parameters.AddWithValue("_correlativo", repo.Correlativo);
                oParam = cmd.Parameters.AddWithValue("_monto", repo.Monto);
                oParam = cmd.Parameters.AddWithValue("_estadosunat", repo.Estadosunat);
                oParam = cmd.Parameters.AddWithValue("_mensajesunat", repo.Mensajesunat);
                oParam = cmd.Parameters.AddWithValue("_docpdf", repo.Pdf);
                oParam = cmd.Parameters.AddWithValue("_docxml", repo.Xml);
                oParam = cmd.Parameters.AddWithValue("_nombredoc", repo.Nombredoc);
                oParam = cmd.Parameters.AddWithValue("_usuario", repo.Usuario);
                oParam = cmd.Parameters.AddWithValue("_codEmpresa", repo.CodEmpresa);
                oParam = cmd.Parameters.AddWithValue("_codSucursal", repo.CodSucursal);
                oParam = cmd.Parameters.AddWithValue("_codAlmacen", repo.CodAlmacen);
                oParam = cmd.Parameters.AddWithValue("_codFacturaVenta", repo.CodFacturaVenta);
                oParam = cmd.Parameters.AddWithValue("TipDocRelacion_ex", repo.TipDocRelacion);
                oParam = cmd.Parameters.AddWithValue("_resultado", 0);
                oParam.Direction = ParameterDirection.Output;
                int x = cmd.ExecuteNonQuery();
                x = Convert.ToInt32(cmd.Parameters["_resultado"].Value);
                mysqltransaccion.Commit();

                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }                

            }
            catch (MySqlException ex)
            {
                if (mysqltransaccion != null)
                {
                    mysqltransaccion.Rollback();
                }
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }


        public List<Entidades.clsRepositorio> buscar_repositorio(Entidades.clsRepositorio repo)
        {
            try
            {
                con.conectarBD();
                consulta = "buscar_repositorio";
                cmd = new MySqlCommand(consulta, con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("_tipdoc", repo.Tipodoc);
                oParam = cmd.Parameters.AddWithValue("_serie", repo.Serie);
                oParam = cmd.Parameters.AddWithValue("_correlativo", repo.Correlativo);
                oParam = cmd.Parameters.AddWithValue("_fechaemision", repo.Fechaemision.ToString("yyyy/MM/dd"));
                oParam = cmd.Parameters.AddWithValue("_monto", repo.Monto);
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    lista = new List<clsRepositorio>();

                    while (dr.Read())
                    {
                        clsrepo = new clsRepositorio();
                        clsrepo.Repoid = (int)dr["repositorioid"];
                        clsrepo.Tipodoc = (int)dr["tipdoc"];
                        clsrepo.Fechaemision = DateTime.Parse(dr["fechaemision"].ToString()).Date;
                        clsrepo.Serie = (string)dr["serie"];
                        clsrepo.Correlativo = (string)dr["correlativo"];
                        clsrepo.Monto = (decimal)dr["monto"];
                        clsrepo.Estadosunat = (string)dr["estadosunat"];
                        clsrepo.Mensajesunat = (string)dr["mensajesunat"];
                        clsrepo.Pdf = (byte[])dr["docpdf"];
                        clsrepo.Xml = (byte[])dr["docxml"];
                        clsrepo.Nombredoc = (string)dr["nombredoc"];
                        clsrepo.Usuario = (int)dr["usuario"];
                        clsrepo.Fechaemision = DateTime.Parse(dr["fecharegistro"].ToString());
                        lista.Add(clsrepo);
                    }

                }

                return lista;
                

            }
            catch (MySqlException ex)
            {
                return lista;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }


        public List<clsRepositorio> listar_repositorio(String estado, Int32 codsucu, Int32 codalma, DateTime fecha)
        {
            try
            {
                lista = null;
                con.conectarBD();
                consulta = "listar_repositorio";
                cmd = new MySqlCommand(consulta, con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 25000;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("_estadosunat", estado);
                oParam = cmd.Parameters.AddWithValue("_codSucursal", codsucu);
                oParam = cmd.Parameters.AddWithValue("_codAlmacen", codalma);
                oParam = cmd.Parameters.AddWithValue("_fecharegistro", fecha);
                //oParam = cmd.Parameters.AddWithValue("codFacturaVenta_ex", codfac);
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    lista = new List<clsRepositorio>();

                    while (dr.Read())
                    {
                        clsrepo = new clsRepositorio();
                        clsrepo.Repoid = (int)dr["repositorioid"];
                        clsrepo.Tipodoc = (int)dr["tipdoc"];
                        clsrepo.Fechaemision = DateTime.Parse(dr["fechaemision"].ToString());
                        clsrepo.Serie = (string)dr["serie"];
                        clsrepo.Correlativo = (string)dr["correlativo"];
                        clsrepo.Monto = (decimal)dr["monto"];
                        clsrepo.Estadosunat = (string)dr["estadosunat"];
                        clsrepo.Mensajesunat = (string)dr["mensajesunat"];
                        clsrepo.Pdf = (byte[])dr["docpdf"];
                        clsrepo.Xml = (byte[])dr["docxml"];
                        clsrepo.Nombredoc = (string)dr["nombredoc"];
                        clsrepo.Usuario = (int)dr["usuario"];
                        clsrepo.Fechaemision = DateTime.Parse(dr["fecharegistro"].ToString());
                        lista.Add(clsrepo);
                    }

                }
                return lista;
            }
            catch (MySqlException ex)
            {
                return lista;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }


        public bool actualiza_repositorio(clsRepositorio repo)
        {
            try
            {
                con.conectarBD();

                mysqltransaccion = con.conector.BeginTransaction();
                consulta = "actualiza_estadosunat_repositorio";
                cmd = new MySqlCommand(consulta, con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("_repositorioid", repo.Repoid);
                oParam = cmd.Parameters.AddWithValue("_estadosunat", repo.Estadosunat);
                oParam = cmd.Parameters.AddWithValue("_mensajesunat", repo.Mensajesunat);
                oParam = cmd.Parameters.AddWithValue("_cdrzip", repo.CDR);
                oParam = cmd.Parameters.AddWithValue("_resultado", 0);
                oParam.Direction = ParameterDirection.Output;
                int x = cmd.ExecuteNonQuery();
                x = Convert.ToInt32(cmd.Parameters["_resultado"].Value);
                mysqltransaccion.Commit();

                if (x != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (MySqlException ex)
            {
                if (mysqltransaccion != null)
                {
                    mysqltransaccion.Rollback();
                }
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public Boolean ActualizaCorrelativoDocResp(Int32 codtipodoc, Int32 codalma)
        {
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("ActualizaCorrelativoDocResp", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("codDocumento_ex", codtipodoc);
                oParam = cmd.Parameters.AddWithValue("codAlmacen_ex", codalma); 
                int x = cmd.ExecuteNonQuery();
                if (x != 0) { return true; }
                else { return false; }
            }
            catch (MySqlException ex)
            {
                if (mysqltransaccion != null)
                {
                    mysqltransaccion.Rollback();
                }
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public List<clsRepositorio> listar_repositorio_Enviados(String estado, Int32 codsucu, Int32 codalma, DateTime fecha)
        {
            try
            {
                lista = null;
                con.conectarBD();
                consulta = "listar_repositorio_Enviados";
                cmd = new MySqlCommand(consulta, con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("_estadosunat", estado);
                oParam = cmd.Parameters.AddWithValue("_codSucursal", codsucu);
                oParam = cmd.Parameters.AddWithValue("_codAlmacen", codalma);
                oParam = cmd.Parameters.AddWithValue("_fecharegistro", fecha);
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    lista = new List<clsRepositorio>();

                    while (dr.Read())
                    {
                        clsrepo = new clsRepositorio();
                        clsrepo.Repoid = (int)dr["repositorioid"];
                        clsrepo.Tipodoc = (int)dr["tipdoc"];
                        clsrepo.Fechaemision = DateTime.Parse(dr["fechaemision"].ToString());
                        clsrepo.Serie = (string)dr["serie"];
                        clsrepo.Correlativo = (string)dr["correlativo"];
                        clsrepo.Monto = (decimal)dr["monto"];
                        clsrepo.Estadosunat = (string)dr["estadosunat"];
                        clsrepo.Mensajesunat = (string)dr["mensajesunat"];
                        clsrepo.Pdf = (byte[])dr["docpdf"];
                        clsrepo.Xml = (byte[])dr["docxml"];
                        clsrepo.Nombredoc = (string)dr["nombredoc"];
                        clsrepo.Usuario = (int)dr["usuario"];
                        clsrepo.Fechaemision = DateTime.Parse(dr["fecharegistro"].ToString());
                        lista.Add(clsrepo);
                    }

                }
                return lista;
            }
            catch (MySqlException ex)
            {
                return lista;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public int ultimoCorrelativoResumen(DateTime fecha)
        {
            try
            { 
                con.conectarBD();

                cmd = new MySqlCommand("ultimoCorrelativoResumen", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("fecha", fecha);
                return Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (MySqlException ex)
            {
                return 0;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listaBoletasResumen(DateTime fecha, int codalma)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("listaBoletasResumen", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("fecha", fecha);
                cmd.Parameters.AddWithValue("codalma", codalma);
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                return null;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public bool registra_documento_electronico(clsDocumentoElectronico doc)
        {
            try
            {
                con.conectarBD();

                mysqltransaccion = con.conector.BeginTransaction();
                consulta = "registroDocumentoElectronico";
                cmd = new MySqlCommand(consulta, con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("_tipdoc", doc.Tipodoc);
                oParam = cmd.Parameters.AddWithValue("_fechaemision", doc.Fechaemision.ToString("yyyy/MM/dd"));
                oParam = cmd.Parameters.AddWithValue("_serie", doc.Serie);
                oParam = cmd.Parameters.AddWithValue("_correlativo", doc.Correlativo);
                oParam = cmd.Parameters.AddWithValue("_monto", doc.Monto);
                oParam = cmd.Parameters.AddWithValue("_estadosunat", doc.Estadosunat);
                oParam = cmd.Parameters.AddWithValue("_mensajesunat", doc.Mensajesunat);
                oParam = cmd.Parameters.AddWithValue("_hash", doc.Hash);
                oParam = cmd.Parameters.AddWithValue("_nombredoc", doc.Nombredoc);
                oParam = cmd.Parameters.AddWithValue("_codusuario", doc.Usuario);
                oParam = cmd.Parameters.AddWithValue("_codEmpresa", doc.CodEmpresa);
                oParam = cmd.Parameters.AddWithValue("_codSucursal", doc.CodSucursal);
                oParam = cmd.Parameters.AddWithValue("_codAlmacen", doc.CodAlmacen);
                oParam = cmd.Parameters.AddWithValue("_codFacturaVenta", doc.CodFacturaVenta);
                oParam = cmd.Parameters.AddWithValue("_resultado", 0);
                oParam.Direction = ParameterDirection.Output;
                int x = cmd.ExecuteNonQuery();
                x = Convert.ToInt32(cmd.Parameters["_resultado"].Value);
                

                if (x != 0)
                {
                    mysqltransaccion.Commit();
                    return true;
                }
                else
                {
                    mysqltransaccion.Rollback();
                    return false;
                }

            }
            catch (MySqlException ex)
            {
                if (mysqltransaccion != null)
                {
                    mysqltransaccion.Rollback();
                }
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public bool registra_ResumenDiario(clsResumenDiario doc, DataTable detalle)
        {
            bool rpta = true;

            using (var Scope = new TransactionScope())
            {
                try
                {
                    con.conectarBD();
                    cmd = new MySqlCommand("registroResumenDiario", con.conector);
                    cmd.CommandType = CommandType.StoredProcedure;
                    MySqlParameter oParam;
                    oParam = cmd.Parameters.AddWithValue("_tipdoc", doc.Tipodoc);
                    oParam = cmd.Parameters.AddWithValue("_fechaemision", doc.Fechaemision.ToString("yyyy/MM/dd"));
                    oParam = cmd.Parameters.AddWithValue("_fechadocumentos", doc.Fechadocumentos.ToString("yyyy/MM/dd"));
                    oParam = cmd.Parameters.AddWithValue("_correlativo", doc.Correlativo);
                    oParam = cmd.Parameters.AddWithValue("_monto", doc.Monto);
                    oParam = cmd.Parameters.AddWithValue("_estadosunat", doc.Estadosunat);
                    oParam = cmd.Parameters.AddWithValue("_mensajesunat", doc.Mensajesunat);
                    oParam = cmd.Parameters.AddWithValue("_hash", doc.Hash);
                    oParam = cmd.Parameters.AddWithValue("_nombredoc", doc.Nombredoc);
                    oParam = cmd.Parameters.AddWithValue("_codusuario", doc.Usuario);
                    oParam = cmd.Parameters.AddWithValue("_cantidad", doc.CantidadDocs);
                    oParam = cmd.Parameters.AddWithValue("_codEmpresa", doc.CodEmpresa);
                    oParam = cmd.Parameters.AddWithValue("_codSucursal", doc.CodSucursal);
                    oParam = cmd.Parameters.AddWithValue("_codAlmacen", doc.CodAlmacen);
                    oParam = cmd.Parameters.AddWithValue("_resultado", 0);
                    oParam.Direction = ParameterDirection.Output;
                    int x = cmd.ExecuteNonQuery();

                    doc.Resumenid = Convert.ToInt32(cmd.Parameters["_resultado"].Value);

                    if (x == 0)
                    {
                        rpta = false;
                    }

                    if(rpta==false)
                    {
                        Transaction.Current.Rollback();
                        Scope.Dispose();
                        return rpta;
                    }
                    else
                    {
                       
                        foreach (DataRow det in detalle.Rows)
                        {
                            cmd = new MySqlCommand("registroDetalleResumenMasivo", con.conector);
                            cmd.CommandType = CommandType.StoredProcedure;
                            MySqlParameter oParamD;
                            oParamD = cmd.Parameters.AddWithValue("_idresumen", doc.Resumenid);
                            oParamD = cmd.Parameters.AddWithValue("_idfacturaventa", det["codboleta"]);
                            oParamD = cmd.Parameters.AddWithValue("newid", 0);
                            oParamD.Direction = ParameterDirection.Output;

                            int filas = cmd.ExecuteNonQuery();

                            int cod = Convert.ToInt32(cmd.Parameters["newid"].Value);

                            if (cod == 0 || cod==-1)
                            {
                                rpta= false;
                                break;
                            }

                        }

                        if (!rpta)
                        {
                            Transaction.Current.Rollback();
                            Scope.Dispose();
                            return rpta;
                        }
                        else
                        {
                            Scope.Complete();
                            Scope.Dispose();
                        }

                        return rpta;
                    }

                }
                catch (MySqlException ex)
                {
                    Transaction.Current.Rollback();
                    Scope.Dispose();
                    rpta = false;
                    return rpta;
                }
                finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
            }
        }

        public DataTable listaResumenes(int codalma,DateTime desde, DateTime hasta)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("listaResumenes", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("codalma", codalma);
                cmd.Parameters.AddWithValue("desde", desde);
                cmd.Parameters.AddWithValue("hasta", hasta);
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                return null;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public clsResumenDiario getResumenxId(int codresumen)
        {
            try
            {
                clsResumenDiario res=null;
                con.conectarBD();
                consulta = "getResumenxId";
                cmd = new MySqlCommand(consulta, con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("codresumen", codresumen);
                dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    res = new clsResumenDiario();

                    while (dr.Read())
                    {
                        res = new clsResumenDiario();
                        res.Resumenid = (int)dr["resumenid"];
                        res.Tipodoc = (string)dr["tipdoc"];
                        res.Fechaemision = DateTime.Parse(dr["fechaemision"].ToString()).Date;
                        res.Fechadocumentos = DateTime.Parse(dr["fechadocumentos"].ToString()).Date;
                        res.Correlativo = (string)dr["correlativo"];
                        res.Monto = (decimal)dr["monto"];
                        res.Estadosunat = (string)dr["estadosunat"];
                        res.Mensajesunat = (string)dr["mensajesunat"];
                        res.Hash = (string)dr["codigoHash"];
                        res.Nombredoc = (string)dr["nombredoc"];
                        res.Usuario = (int)dr["codusuario"];
                        res.Fecharegistro = DateTime.Parse(dr["fecharegistro"].ToString()).Date;
                        if (!DBNull.Value.Equals(dr["fechaenvio"]))
                        {
                            res.Fechaenvio = DateTime.Parse(dr["fechaenvio"].ToString()).Date;
                        }
                        else
                        {
                            res.Fechaenvio = new DateTime(1900,01,01);
                        }
                        
                        res.CantidadDocs= (int)dr["cantidadDocumentos"];
                        res.CodEmpresa = (int)dr["codempresa"];
                        res.CodSucursal = (int)dr["codsucursal"];
                        res.CodAlmacen = (int)dr["codalmacen"];
                        

                        if (!DBNull.Value.Equals(dr["ticket"]))
                        {
                            res.Ticket = (string)dr["ticket"];
                        }
                        else
                        {
                            res.Ticket = string.Empty;
                        }
                    }
                }

                return res;

            }
            catch (MySqlException ex)
            {
                return null;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public DataTable listaBoletasResumenxId(int codresumen)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("listaBoletasResumenxId", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("codresumen", codresumen);
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                return null;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public bool updateResumenxId(clsResumenDiario doc)
        {
            bool rpta = true;

            using (var Scope = new TransactionScope())
            {
                try
                {
                    con.conectarBD();

                    consulta = "updateResumenxId";
                    cmd = new MySqlCommand(consulta, con.conector);
                    cmd.CommandType = CommandType.StoredProcedure;
                    MySqlParameter oParam;
                    oParam = cmd.Parameters.AddWithValue("_codresumen", doc.Resumenid);
                    oParam = cmd.Parameters.AddWithValue("_hash", doc.Hash);
                    oParam = cmd.Parameters.AddWithValue("_estado", doc.Estadosunat);
                    oParam = cmd.Parameters.AddWithValue("_mensaje", doc.Mensajesunat);
                    oParam = cmd.Parameters.AddWithValue("_envio", doc.Fechaenvio);
                    oParam = cmd.Parameters.AddWithValue("_ticket", doc.Ticket);
                    int x = cmd.ExecuteNonQuery();
                    

                    if (x == 0 || x==-1)
                    {
                       rpta=false;
                    }

                    if (rpta)
                    {
                        Scope.Complete();
                        Scope.Dispose();
                    }
                    else
                    {
                        Transaction.Current.Rollback();
                        Scope.Dispose();
                        return rpta;
                    }

                    return rpta;

                }
                catch (MySqlException ex)
                {
                    Transaction.Current.Rollback();
                    Scope.Dispose();
                    rpta = false;
                    return rpta;
                }
                finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
            }
        }

        public DataTable listaTicketsxEstado(DateTime desde, DateTime hasta, string estado)
        {
            try
            {
                tabla = new DataTable();
                con.conectarBD();
                cmd = new MySqlCommand("listaTicketsxEstado", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("desde", desde);
                cmd.Parameters.AddWithValue("hasta", hasta);
                cmd.Parameters.AddWithValue("estado", estado);
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(tabla);
                return tabla;

            }
            catch (Exception ex)
            {
                return null;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }

        public bool updateRepositorioBoletas(clsResumenDiario doc)
        {
            bool rpta = true;
            try
            {
                con.conectarBD();

                consulta = "updateRepositorioBoletas";
                cmd = new MySqlCommand(consulta, con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameter oParam;
                oParam = cmd.Parameters.AddWithValue("codresumen", doc.Resumenid);
                int x = cmd.ExecuteNonQuery();

                if(x!=0)
                {
                    rpta=true;
                }
                else
                {
                    rpta = false;
                }

                return rpta;

            }
            catch (MySqlException ex)
            {
                return rpta;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
