﻿namespace SIGEFA.Formularios
{
    partial class frmResumenDiario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmResumenDiario));
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn27 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn28 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn29 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn30 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn31 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn32 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn33 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn34 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn35 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn36 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn37 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn38 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn39 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn40 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            this.windows8Theme1 = new Telerik.WinControls.Themes.Windows8Theme();
            this.dgvVentasResumen = new Telerik.WinControls.UI.RadGridView();
            this.dtpFecha = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radPageView1 = new Telerik.WinControls.UI.RadPageView();
            this.tab1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.lblMonto = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.cmbAlmacenes = new Telerik.WinControls.UI.RadDropDownList();
            this.btnActualizar = new Telerik.WinControls.UI.RadButton();
            this.btnLimpiar = new Telerik.WinControls.UI.RadButton();
            this.btnGenerar = new Telerik.WinControls.UI.RadButton();
            this.tab2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.dtpHasta = new Telerik.WinControls.UI.RadDateTimePicker();
            this.dtpDesde = new Telerik.WinControls.UI.RadDateTimePicker();
            this.btnListarResumenes = new Telerik.WinControls.UI.RadButton();
            this.btnEnviarResumen = new Telerik.WinControls.UI.RadButton();
            this.lblDocSeleccionado = new Telerik.WinControls.UI.RadLabel();
            this.dgvResumenes = new Telerik.WinControls.UI.RadGridView();
            this.tab3 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.cmbEstado = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.dtpTicketHasta = new Telerik.WinControls.UI.RadDateTimePicker();
            this.dtpTicketDesde = new Telerik.WinControls.UI.RadDateTimePicker();
            this.btnConsultarTicket = new Telerik.WinControls.UI.RadButton();
            this.btnListarTicket = new Telerik.WinControls.UI.RadButton();
            this.lblTicketSeleccion = new Telerik.WinControls.UI.RadLabel();
            this.dgvTickets = new Telerik.WinControls.UI.RadGridView();
            this.materialTheme1 = new Telerik.WinControls.Themes.MaterialTheme();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVentasResumen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVentasResumen.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFecha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).BeginInit();
            this.radPageView1.SuspendLayout();
            this.tab1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblMonto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbAlmacenes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnActualizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnLimpiar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnGenerar)).BeginInit();
            this.tab2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpHasta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDesde)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnListarResumenes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEnviarResumen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDocSeleccionado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResumenes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResumenes.MasterTemplate)).BeginInit();
            this.tab3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbEstado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTicketHasta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTicketDesde)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnConsultarTicket)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnListarTicket)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTicketSeleccion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTickets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTickets.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvVentasResumen
            // 
            this.dgvVentasResumen.AutoSizeRows = true;
            resources.ApplyResources(this.dgvVentasResumen, "dgvVentasResumen");
            // 
            // 
            // 
            this.dgvVentasResumen.MasterTemplate.AllowAddNewRow = false;
            this.dgvVentasResumen.MasterTemplate.AllowDragToGroup = false;
            this.dgvVentasResumen.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn1.FieldName = "codboleta";
            resources.ApplyResources(gridViewTextBoxColumn1, "gridViewTextBoxColumn1");
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "codboleta";
            gridViewTextBoxColumn2.FieldName = "fecha";
            resources.ApplyResources(gridViewTextBoxColumn2, "gridViewTextBoxColumn2");
            gridViewTextBoxColumn2.Name = "fecha";
            gridViewTextBoxColumn2.Width = 104;
            gridViewTextBoxColumn3.FieldName = "documento";
            resources.ApplyResources(gridViewTextBoxColumn3, "gridViewTextBoxColumn3");
            gridViewTextBoxColumn3.IsVisible = false;
            gridViewTextBoxColumn3.Name = "documento";
            gridViewTextBoxColumn4.FieldName = "numeroDocumento";
            resources.ApplyResources(gridViewTextBoxColumn4, "gridViewTextBoxColumn4");
            gridViewTextBoxColumn4.Name = "numeroDocumento";
            gridViewTextBoxColumn4.Width = 104;
            gridViewTextBoxColumn5.FieldName = "nombreCliente";
            resources.ApplyResources(gridViewTextBoxColumn5, "gridViewTextBoxColumn5");
            gridViewTextBoxColumn5.Name = "nombreCliente";
            gridViewTextBoxColumn5.Width = 104;
            gridViewTextBoxColumn6.FieldName = "documentoCliente";
            resources.ApplyResources(gridViewTextBoxColumn6, "gridViewTextBoxColumn6");
            gridViewTextBoxColumn6.Name = "documentoCliente";
            gridViewTextBoxColumn6.Width = 104;
            gridViewTextBoxColumn7.FieldName = "moneda";
            resources.ApplyResources(gridViewTextBoxColumn7, "gridViewTextBoxColumn7");
            gridViewTextBoxColumn7.IsVisible = false;
            gridViewTextBoxColumn7.Name = "moneda";
            gridViewTextBoxColumn8.FieldName = "total";
            resources.ApplyResources(gridViewTextBoxColumn8, "gridViewTextBoxColumn8");
            gridViewTextBoxColumn8.Name = "total";
            gridViewTextBoxColumn8.Width = 104;
            gridViewTextBoxColumn9.FieldName = "gravadas";
            resources.ApplyResources(gridViewTextBoxColumn9, "gridViewTextBoxColumn9");
            gridViewTextBoxColumn9.IsVisible = false;
            gridViewTextBoxColumn9.Name = "gravadas";
            gridViewTextBoxColumn10.FieldName = "exoneradas";
            resources.ApplyResources(gridViewTextBoxColumn10, "gridViewTextBoxColumn10");
            gridViewTextBoxColumn10.IsVisible = false;
            gridViewTextBoxColumn10.Name = "exoneradas";
            gridViewTextBoxColumn11.FieldName = "inafectas";
            resources.ApplyResources(gridViewTextBoxColumn11, "gridViewTextBoxColumn11");
            gridViewTextBoxColumn11.IsVisible = false;
            gridViewTextBoxColumn11.Name = "inafectas";
            gridViewTextBoxColumn12.FieldName = "gratuitas";
            resources.ApplyResources(gridViewTextBoxColumn12, "gridViewTextBoxColumn12");
            gridViewTextBoxColumn12.IsVisible = false;
            gridViewTextBoxColumn12.Name = "gratuitas";
            gridViewTextBoxColumn13.FieldName = "igv";
            resources.ApplyResources(gridViewTextBoxColumn13, "gridViewTextBoxColumn13");
            gridViewTextBoxColumn13.IsVisible = false;
            gridViewTextBoxColumn13.Name = "igv";
            gridViewTextBoxColumn14.FieldName = "formapago";
            resources.ApplyResources(gridViewTextBoxColumn14, "gridViewTextBoxColumn14");
            gridViewTextBoxColumn14.Name = "formapago";
            gridViewTextBoxColumn14.Width = 104;
            gridViewTextBoxColumn15.FieldName = "fechapago";
            resources.ApplyResources(gridViewTextBoxColumn15, "gridViewTextBoxColumn15");
            gridViewTextBoxColumn15.IsVisible = false;
            gridViewTextBoxColumn15.Name = "fechapago";
            gridViewTextBoxColumn16.FieldName = "codsunat";
            resources.ApplyResources(gridViewTextBoxColumn16, "gridViewTextBoxColumn16");
            gridViewTextBoxColumn16.IsVisible = false;
            gridViewTextBoxColumn16.Name = "codsunat";
            this.dgvVentasResumen.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16});
            this.dgvVentasResumen.MasterTemplate.EnableGrouping = false;
            this.dgvVentasResumen.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.dgvVentasResumen.Name = "dgvVentasResumen";
            this.dgvVentasResumen.ReadOnly = true;
            this.dgvVentasResumen.ShowGroupPanel = false;
            this.dgvVentasResumen.ThemeName = "Material";
            this.dgvVentasResumen.Click += new System.EventHandler(this.dgvVentasResumen_Click);
            // 
            // dtpFecha
            // 
            this.dtpFecha.CalendarSize = new System.Drawing.Size(290, 320);
            this.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            resources.ApplyResources(this.dtpFecha, "dtpFecha");
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.TabStop = false;
            this.dtpFecha.ThemeName = "Material";
            this.dtpFecha.Value = new System.DateTime(2019, 9, 25, 19, 32, 3, 215);
            // 
            // radLabel1
            // 
            resources.ApplyResources(this.radLabel1, "radLabel1");
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.ThemeName = "Material";
            // 
            // radPageView1
            // 
            this.radPageView1.Controls.Add(this.tab1);
            this.radPageView1.Controls.Add(this.tab2);
            this.radPageView1.Controls.Add(this.tab3);
            this.radPageView1.DefaultPage = this.tab1;
            resources.ApplyResources(this.radPageView1, "radPageView1");
            this.radPageView1.Name = "radPageView1";
            this.radPageView1.SelectedPage = this.tab1;
            this.radPageView1.ThemeName = "Material";
            // 
            // tab1
            // 
            this.tab1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tab1.Controls.Add(this.lblMonto);
            this.tab1.Controls.Add(this.radLabel3);
            this.tab1.Controls.Add(this.radLabel2);
            this.tab1.Controls.Add(this.cmbAlmacenes);
            this.tab1.Controls.Add(this.btnActualizar);
            this.tab1.Controls.Add(this.btnLimpiar);
            this.tab1.Controls.Add(this.btnGenerar);
            this.tab1.Controls.Add(this.radLabel1);
            this.tab1.Controls.Add(this.dgvVentasResumen);
            this.tab1.Controls.Add(this.dtpFecha);
            this.tab1.ItemSize = new System.Drawing.SizeF(166F, 49F);
            resources.ApplyResources(this.tab1, "tab1");
            this.tab1.Name = "tab1";
            // 
            // lblMonto
            // 
            resources.ApplyResources(this.lblMonto, "lblMonto");
            this.lblMonto.Name = "lblMonto";
            this.lblMonto.ThemeName = "Material";
            // 
            // radLabel3
            // 
            resources.ApplyResources(this.radLabel3, "radLabel3");
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.ThemeName = "Material";
            // 
            // radLabel2
            // 
            resources.ApplyResources(this.radLabel2, "radLabel2");
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.ThemeName = "Material";
            // 
            // cmbAlmacenes
            // 
            resources.ApplyResources(this.cmbAlmacenes, "cmbAlmacenes");
            this.cmbAlmacenes.Name = "cmbAlmacenes";
            this.cmbAlmacenes.ThemeName = "Material";
            // 
            // btnActualizar
            // 
            this.btnActualizar.Image = global::SIGEFA.Properties.Resources.actualizar24;
            resources.ApplyResources(this.btnActualizar, "btnActualizar");
            this.btnActualizar.Name = "btnActualizar";
            this.btnActualizar.ThemeName = "Material";
            this.btnActualizar.Click += new System.EventHandler(this.btnActualizar_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.btnLimpiar, "btnLimpiar");
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.ThemeName = "Material";
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnGenerar
            // 
            this.btnGenerar.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.btnGenerar, "btnGenerar");
            this.btnGenerar.Name = "btnGenerar";
            this.btnGenerar.ThemeName = "Material";
            this.btnGenerar.Click += new System.EventHandler(this.btnGenerar_Click);
            // 
            // tab2
            // 
            this.tab2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tab2.Controls.Add(this.radLabel6);
            this.tab2.Controls.Add(this.radLabel5);
            this.tab2.Controls.Add(this.dtpHasta);
            this.tab2.Controls.Add(this.dtpDesde);
            this.tab2.Controls.Add(this.btnListarResumenes);
            this.tab2.Controls.Add(this.btnEnviarResumen);
            this.tab2.Controls.Add(this.lblDocSeleccionado);
            this.tab2.Controls.Add(this.dgvResumenes);
            this.tab2.ItemSize = new System.Drawing.SizeF(143F, 49F);
            resources.ApplyResources(this.tab2, "tab2");
            this.tab2.Name = "tab2";
            // 
            // radLabel6
            // 
            resources.ApplyResources(this.radLabel6, "radLabel6");
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.ThemeName = "Material";
            // 
            // radLabel5
            // 
            resources.ApplyResources(this.radLabel5, "radLabel5");
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.ThemeName = "Material";
            // 
            // dtpHasta
            // 
            this.dtpHasta.CalendarSize = new System.Drawing.Size(290, 320);
            this.dtpHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            resources.ApplyResources(this.dtpHasta, "dtpHasta");
            this.dtpHasta.Name = "dtpHasta";
            this.dtpHasta.TabStop = false;
            this.dtpHasta.ThemeName = "Material";
            this.dtpHasta.Value = new System.DateTime(2019, 9, 25, 0, 0, 0, 0);
            // 
            // dtpDesde
            // 
            this.dtpDesde.CalendarSize = new System.Drawing.Size(290, 320);
            this.dtpDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            resources.ApplyResources(this.dtpDesde, "dtpDesde");
            this.dtpDesde.Name = "dtpDesde";
            this.dtpDesde.TabStop = false;
            this.dtpDesde.ThemeName = "Material";
            this.dtpDesde.Value = new System.DateTime(2019, 9, 25, 0, 0, 0, 0);
            // 
            // btnListarResumenes
            // 
            resources.ApplyResources(this.btnListarResumenes, "btnListarResumenes");
            this.btnListarResumenes.Name = "btnListarResumenes";
            this.btnListarResumenes.ThemeName = "Material";
            this.btnListarResumenes.Click += new System.EventHandler(this.radButton4_Click);
            // 
            // btnEnviarResumen
            // 
            resources.ApplyResources(this.btnEnviarResumen, "btnEnviarResumen");
            this.btnEnviarResumen.Name = "btnEnviarResumen";
            this.btnEnviarResumen.ThemeName = "Material";
            this.btnEnviarResumen.Click += new System.EventHandler(this.btnEnviarResumen_Click);
            // 
            // lblDocSeleccionado
            // 
            resources.ApplyResources(this.lblDocSeleccionado, "lblDocSeleccionado");
            this.lblDocSeleccionado.Name = "lblDocSeleccionado";
            this.lblDocSeleccionado.ThemeName = "Material";
            // 
            // dgvResumenes
            // 
            this.dgvResumenes.AutoSizeRows = true;
            resources.ApplyResources(this.dgvResumenes, "dgvResumenes");
            // 
            // 
            // 
            this.dgvResumenes.MasterTemplate.AllowAddNewRow = false;
            this.dgvResumenes.MasterTemplate.AllowDragToGroup = false;
            this.dgvResumenes.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn17.FieldName = "resumenid";
            resources.ApplyResources(gridViewTextBoxColumn17, "gridViewTextBoxColumn17");
            gridViewTextBoxColumn17.IsVisible = false;
            gridViewTextBoxColumn17.Name = "idresumen";
            gridViewTextBoxColumn18.FieldName = "nombredoc";
            resources.ApplyResources(gridViewTextBoxColumn18, "gridViewTextBoxColumn18");
            gridViewTextBoxColumn18.Name = "nombredoc";
            gridViewTextBoxColumn18.Width = 94;
            gridViewTextBoxColumn19.FieldName = "ticket";
            resources.ApplyResources(gridViewTextBoxColumn19, "gridViewTextBoxColumn19");
            gridViewTextBoxColumn19.IsVisible = false;
            gridViewTextBoxColumn19.Name = "nroticket";
            gridViewTextBoxColumn19.Width = 103;
            gridViewTextBoxColumn20.FieldName = "estadosunat";
            resources.ApplyResources(gridViewTextBoxColumn20, "gridViewTextBoxColumn20");
            gridViewTextBoxColumn20.Name = "estadosunat";
            gridViewTextBoxColumn20.Width = 141;
            gridViewTextBoxColumn21.FieldName = "mensajesunat";
            resources.ApplyResources(gridViewTextBoxColumn21, "gridViewTextBoxColumn21");
            gridViewTextBoxColumn21.Name = "mensajesunat";
            gridViewTextBoxColumn21.Width = 141;
            gridViewTextBoxColumn22.FieldName = "resumenid";
            resources.ApplyResources(gridViewTextBoxColumn22, "gridViewTextBoxColumn22");
            gridViewTextBoxColumn22.IsVisible = false;
            gridViewTextBoxColumn22.Name = "resumenid";
            gridViewTextBoxColumn22.Width = 88;
            gridViewTextBoxColumn23.FieldName = "fechaemision";
            resources.ApplyResources(gridViewTextBoxColumn23, "gridViewTextBoxColumn23");
            gridViewTextBoxColumn23.Name = "fechaemision";
            gridViewTextBoxColumn23.Width = 65;
            gridViewTextBoxColumn24.FieldName = "fechadocumentos";
            resources.ApplyResources(gridViewTextBoxColumn24, "gridViewTextBoxColumn24");
            gridViewTextBoxColumn24.Name = "fechadocumentos";
            gridViewTextBoxColumn24.Width = 65;
            gridViewTextBoxColumn25.FieldName = "monto";
            resources.ApplyResources(gridViewTextBoxColumn25, "gridViewTextBoxColumn25");
            gridViewTextBoxColumn25.Name = "monto";
            gridViewTextBoxColumn25.Width = 62;
            gridViewTextBoxColumn26.FieldName = "cantidadDocumentos";
            resources.ApplyResources(gridViewTextBoxColumn26, "gridViewTextBoxColumn26");
            gridViewTextBoxColumn26.Name = "cantidadDocumentos";
            gridViewTextBoxColumn26.Width = 56;
            gridViewTextBoxColumn27.FieldName = "codigoHash";
            resources.ApplyResources(gridViewTextBoxColumn27, "gridViewTextBoxColumn27");
            gridViewTextBoxColumn27.IsVisible = false;
            gridViewTextBoxColumn27.Name = "codigoHash";
            gridViewTextBoxColumn27.Width = 47;
            gridViewTextBoxColumn28.FieldName = "tipdoc";
            resources.ApplyResources(gridViewTextBoxColumn28, "gridViewTextBoxColumn28");
            gridViewTextBoxColumn28.IsVisible = false;
            gridViewTextBoxColumn28.Name = "tipdoc";
            gridViewTextBoxColumn28.Width = 46;
            gridViewTextBoxColumn29.FieldName = "correlativo";
            resources.ApplyResources(gridViewTextBoxColumn29, "gridViewTextBoxColumn29");
            gridViewTextBoxColumn29.IsVisible = false;
            gridViewTextBoxColumn29.Name = "correlativo";
            gridViewTextBoxColumn29.Width = 47;
            gridViewTextBoxColumn30.FieldName = "codusuario";
            resources.ApplyResources(gridViewTextBoxColumn30, "gridViewTextBoxColumn30");
            gridViewTextBoxColumn30.IsVisible = false;
            gridViewTextBoxColumn30.Name = "codusuario";
            gridViewTextBoxColumn30.Width = 46;
            gridViewTextBoxColumn31.FieldName = "fecharegistro";
            resources.ApplyResources(gridViewTextBoxColumn31, "gridViewTextBoxColumn31");
            gridViewTextBoxColumn31.IsVisible = false;
            gridViewTextBoxColumn31.Name = "fecharegistro";
            gridViewTextBoxColumn31.Width = 39;
            gridViewTextBoxColumn32.FieldName = "fechaenvio";
            resources.ApplyResources(gridViewTextBoxColumn32, "gridViewTextBoxColumn32");
            gridViewTextBoxColumn32.IsVisible = false;
            gridViewTextBoxColumn32.Name = "fechaenvio";
            gridViewTextBoxColumn32.Width = 46;
            gridViewTextBoxColumn33.FieldName = "codempresa";
            resources.ApplyResources(gridViewTextBoxColumn33, "gridViewTextBoxColumn33");
            gridViewTextBoxColumn33.IsVisible = false;
            gridViewTextBoxColumn33.Name = "codempresa";
            gridViewTextBoxColumn33.Width = 49;
            gridViewTextBoxColumn34.FieldName = "codsucursal";
            resources.ApplyResources(gridViewTextBoxColumn34, "gridViewTextBoxColumn34");
            gridViewTextBoxColumn34.IsVisible = false;
            gridViewTextBoxColumn34.Name = "codsucursal";
            gridViewTextBoxColumn34.Width = 51;
            gridViewTextBoxColumn35.FieldName = "codalmacen";
            resources.ApplyResources(gridViewTextBoxColumn35, "gridViewTextBoxColumn35");
            gridViewTextBoxColumn35.IsVisible = false;
            gridViewTextBoxColumn35.Name = "codalmacen";
            gridViewTextBoxColumn35.Width = 46;
            gridViewTextBoxColumn36.FieldName = "ticket";
            resources.ApplyResources(gridViewTextBoxColumn36, "gridViewTextBoxColumn36");
            gridViewTextBoxColumn36.IsVisible = false;
            gridViewTextBoxColumn36.Name = "ticket";
            gridViewTextBoxColumn36.Width = 46;
            this.dgvResumenes.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23,
            gridViewTextBoxColumn24,
            gridViewTextBoxColumn25,
            gridViewTextBoxColumn26,
            gridViewTextBoxColumn27,
            gridViewTextBoxColumn28,
            gridViewTextBoxColumn29,
            gridViewTextBoxColumn30,
            gridViewTextBoxColumn31,
            gridViewTextBoxColumn32,
            gridViewTextBoxColumn33,
            gridViewTextBoxColumn34,
            gridViewTextBoxColumn35,
            gridViewTextBoxColumn36});
            this.dgvResumenes.MasterTemplate.EnableGrouping = false;
            this.dgvResumenes.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.dgvResumenes.Name = "dgvResumenes";
            this.dgvResumenes.ReadOnly = true;
            this.dgvResumenes.ShowGroupPanel = false;
            this.dgvResumenes.ThemeName = "Material";
            this.dgvResumenes.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.dgvResumenes_CellClick);
            // 
            // tab3
            // 
            this.tab3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tab3.Controls.Add(this.radLabel4);
            this.tab3.Controls.Add(this.cmbEstado);
            this.tab3.Controls.Add(this.radLabel7);
            this.tab3.Controls.Add(this.radLabel8);
            this.tab3.Controls.Add(this.dtpTicketHasta);
            this.tab3.Controls.Add(this.dtpTicketDesde);
            this.tab3.Controls.Add(this.btnConsultarTicket);
            this.tab3.Controls.Add(this.btnListarTicket);
            this.tab3.Controls.Add(this.lblTicketSeleccion);
            this.tab3.Controls.Add(this.dgvTickets);
            this.tab3.ItemSize = new System.Drawing.SizeF(167F, 49F);
            resources.ApplyResources(this.tab3, "tab3");
            this.tab3.Name = "tab3";
            this.tab3.Paint += new System.Windows.Forms.PaintEventHandler(this.tab3_Paint);
            // 
            // radLabel4
            // 
            resources.ApplyResources(this.radLabel4, "radLabel4");
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.ThemeName = "Material";
            // 
            // cmbEstado
            // 
            resources.ApplyResources(this.cmbEstado, "cmbEstado");
            this.cmbEstado.Name = "cmbEstado";
            this.cmbEstado.ThemeName = "Material";
            // 
            // radLabel7
            // 
            resources.ApplyResources(this.radLabel7, "radLabel7");
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.ThemeName = "Material";
            // 
            // radLabel8
            // 
            resources.ApplyResources(this.radLabel8, "radLabel8");
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.ThemeName = "Material";
            // 
            // dtpTicketHasta
            // 
            this.dtpTicketHasta.CalendarSize = new System.Drawing.Size(290, 320);
            this.dtpTicketHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            resources.ApplyResources(this.dtpTicketHasta, "dtpTicketHasta");
            this.dtpTicketHasta.Name = "dtpTicketHasta";
            this.dtpTicketHasta.TabStop = false;
            this.dtpTicketHasta.ThemeName = "Material";
            this.dtpTicketHasta.Value = new System.DateTime(2019, 9, 25, 0, 0, 0, 0);
            // 
            // dtpTicketDesde
            // 
            this.dtpTicketDesde.CalendarSize = new System.Drawing.Size(290, 320);
            this.dtpTicketDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            resources.ApplyResources(this.dtpTicketDesde, "dtpTicketDesde");
            this.dtpTicketDesde.Name = "dtpTicketDesde";
            this.dtpTicketDesde.TabStop = false;
            this.dtpTicketDesde.ThemeName = "Material";
            this.dtpTicketDesde.Value = new System.DateTime(2019, 9, 25, 0, 0, 0, 0);
            // 
            // btnConsultarTicket
            // 
            resources.ApplyResources(this.btnConsultarTicket, "btnConsultarTicket");
            this.btnConsultarTicket.Name = "btnConsultarTicket";
            this.btnConsultarTicket.ThemeName = "Material";
            this.btnConsultarTicket.Click += new System.EventHandler(this.btnConsultarTicket_Click);
            // 
            // btnListarTicket
            // 
            resources.ApplyResources(this.btnListarTicket, "btnListarTicket");
            this.btnListarTicket.Name = "btnListarTicket";
            this.btnListarTicket.ThemeName = "Material";
            this.btnListarTicket.Click += new System.EventHandler(this.btnListarTicket_Click);
            // 
            // lblTicketSeleccion
            // 
            resources.ApplyResources(this.lblTicketSeleccion, "lblTicketSeleccion");
            this.lblTicketSeleccion.Name = "lblTicketSeleccion";
            this.lblTicketSeleccion.ThemeName = "Material";
            // 
            // dgvTickets
            // 
            this.dgvTickets.AutoSizeRows = true;
            resources.ApplyResources(this.dgvTickets, "dgvTickets");
            // 
            // 
            // 
            this.dgvTickets.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            gridViewTextBoxColumn37.FieldName = "ticketid";
            resources.ApplyResources(gridViewTextBoxColumn37, "gridViewTextBoxColumn37");
            gridViewTextBoxColumn37.IsVisible = false;
            gridViewTextBoxColumn37.Name = "resumenidticket";
            gridViewTextBoxColumn38.FieldName = "ticket";
            resources.ApplyResources(gridViewTextBoxColumn38, "gridViewTextBoxColumn38");
            gridViewTextBoxColumn38.Name = "ticketnro";
            gridViewTextBoxColumn38.Width = 207;
            gridViewTextBoxColumn39.FieldName = "estadosunat";
            resources.ApplyResources(gridViewTextBoxColumn39, "gridViewTextBoxColumn39");
            gridViewTextBoxColumn39.Name = "estadoticket";
            gridViewTextBoxColumn39.Width = 207;
            gridViewTextBoxColumn40.FieldName = "mensajesunat";
            resources.ApplyResources(gridViewTextBoxColumn40, "gridViewTextBoxColumn40");
            gridViewTextBoxColumn40.Name = "mensajeticket";
            gridViewTextBoxColumn40.Width = 210;
            this.dgvTickets.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn37,
            gridViewTextBoxColumn38,
            gridViewTextBoxColumn39,
            gridViewTextBoxColumn40});
            this.dgvTickets.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.dgvTickets.Name = "dgvTickets";
            this.dgvTickets.ReadOnly = true;
            this.dgvTickets.ShowGroupPanel = false;
            this.dgvTickets.ThemeName = "Material";
            this.dgvTickets.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.dgvTickets_CellClick);
            // 
            // frmResumenDiario
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radPageView1);
            this.Name = "frmResumenDiario";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ThemeName = "Material";
            this.Load += new System.EventHandler(this.frmResumenDiario_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvVentasResumen.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVentasResumen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFecha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).EndInit();
            this.radPageView1.ResumeLayout(false);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblMonto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbAlmacenes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnActualizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnLimpiar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnGenerar)).EndInit();
            this.tab2.ResumeLayout(false);
            this.tab2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpHasta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpDesde)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnListarResumenes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEnviarResumen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDocSeleccionado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResumenes.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResumenes)).EndInit();
            this.tab3.ResumeLayout(false);
            this.tab3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbEstado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTicketHasta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTicketDesde)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnConsultarTicket)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnListarTicket)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTicketSeleccion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTickets.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTickets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.Themes.Windows8Theme windows8Theme1;
        private Telerik.WinControls.UI.RadGridView dgvVentasResumen;
        private Telerik.WinControls.UI.RadDateTimePicker dtpFecha;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadPageView radPageView1;
        private Telerik.WinControls.UI.RadPageViewPage tab1;
        private Telerik.WinControls.UI.RadButton btnLimpiar;
        private Telerik.WinControls.UI.RadButton btnGenerar;
        private Telerik.WinControls.UI.RadPageViewPage tab3;
        private Telerik.WinControls.UI.RadButton btnListarTicket;
        private Telerik.WinControls.UI.RadLabel lblTicketSeleccion;
        private Telerik.WinControls.UI.RadGridView dgvTickets;
        private Telerik.WinControls.Themes.MaterialTheme materialTheme1;
        private Telerik.WinControls.UI.RadButton btnActualizar;
        private Telerik.WinControls.UI.RadDropDownList cmbAlmacenes;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel lblMonto;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadPageViewPage tab2;
        private Telerik.WinControls.UI.RadButton btnEnviarResumen;
        private Telerik.WinControls.UI.RadGridView dgvResumenes;
        private Telerik.WinControls.UI.RadButton btnListarResumenes;
        private Telerik.WinControls.UI.RadButton btnConsultarTicket;
        private Telerik.WinControls.UI.RadLabel lblDocSeleccionado;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadDateTimePicker dtpHasta;
        private Telerik.WinControls.UI.RadDateTimePicker dtpDesde;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadDateTimePicker dtpTicketHasta;
        private Telerik.WinControls.UI.RadDateTimePicker dtpTicketDesde;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadDropDownList cmbEstado;
    }
}
