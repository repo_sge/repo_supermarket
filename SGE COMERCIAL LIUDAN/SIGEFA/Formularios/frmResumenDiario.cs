﻿using SIGEFA.Administradores;
using SIGEFA.SunatFacElec;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;

namespace SIGEFA.Formularios
{
    public partial class frmResumenDiario : Telerik.WinControls.UI.RadForm
    {
        DataTable listaVentas = new DataTable();
        clsAdmRepositorio admRepositorio = new clsAdmRepositorio();
        clsAdmAlmacen admalma = new clsAdmAlmacen();
        Facturacion fact = new Facturacion();
        int codresumen { get; set; }
        string estadosunat { get; set; }
        int idticket { get; set; }

        public frmResumenDiario()
        {
            InitializeComponent();
        }

        private void dgvVentasResumen_Click(object sender, EventArgs e)
        {

        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {

            listadoVentas();
            calculaTotal();

        }

        private void listadoVentas()
        {
            listaVentas = admRepositorio.listaBoletasResumen(dtpFecha.Value, frmLogin.iCodAlmacen);

            dgvVentasResumen.DataSource = listaVentas;
        }

        private void calculaTotal()
        {
            decimal total = 0.00m;

            if (listaVentas.Rows.Count > 0)
            {
                foreach (DataRow dr in listaVentas.Rows)
                {
                    total += Convert.ToDecimal(dr["total"]);
                }
            }

            lblMonto.Text = total.ToString();
        }

        private void frmResumenDiario_Load(object sender, EventArgs e)
        {
            cargaAlmacenes();
            dtpFecha.Value = DateTime.Now.Date;
            dtpDesde.Value = DateTime.Now.Date;
            dtpHasta.Value = DateTime.Now.Date;
            dtpTicketDesde.Value = DateTime.Now.Date;
            dtpTicketHasta.Value = DateTime.Now.Date;

            Dictionary<string, string> estados = new Dictionary<string, string>();
            estados.Add("1", "Espera");
            estados.Add("0", "Aceptado");

            cmbEstado.DisplayMember = "Value";
            cmbEstado.ValueMember = "Key";
            cmbEstado.DataSource = estados;
        }

        private void cargaAlmacenes()
        {
            cmbAlmacenes.DisplayMember = "nombre";
            cmbAlmacenes.ValueMember = "codalmacen";
            cmbAlmacenes.DataSource = admalma.ListaAlmacen2();
            cmbAlmacenes.SelectedValue = frmLogin.iCodAlmacen;
        }

        private async void btnGenerar_Click(object sender, EventArgs e)
        {
            if (listaVentas.Rows.Count > 0)
            {
                fact.GeneraResumen(listaVentas,dtpFecha.Value);
            }
            listaVentas = new DataTable();
            listadoVentas();
            calculaTotal();
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {

            listaVentas = null;
            dgvVentasResumen.DataSource = null;
        }

        private void radButton4_Click(object sender, EventArgs e)
        {
            listadoResumenes();
        }

        private void listadoResumenes()
        {
            dgvResumenes.DataSource = admRepositorio.listaResumenes(frmLogin.iCodAlmacen,dtpDesde.Value.Date,dtpHasta.Value.Date);
        }

        private void dgvResumenes_CellClick(object sender, Telerik.WinControls.UI.GridViewCellEventArgs e)
        {
            if (dgvResumenes.Rows.Count > 0)
            {
                if (e.RowIndex != -1)
                {
                    codresumen = Convert.ToInt32(e.Row.Cells["resumenid"].Value);
                    estadosunat = Convert.ToString(e.Row.Cells["estadosunat"].Value);
                    lblDocSeleccionado.Text = Convert.ToString(e.Row.Cells["nombredoc"].Value);
                }
            }
        }

        private void btnEnviarResumen_Click(object sender, EventArgs e)
        {
            if(codresumen != 0)
            {
                if (estadosunat != "0")
                {
                    fact.envioResumen(codresumen);
                    lblDocSeleccionado.Text = "No hay Doc. seleccionado";
                    codresumen = 0;
                }
                
            }
            listadoResumenes();
        }

        private void btnListarTicket_Click(object sender, EventArgs e)
        {
            listaTickets();
        }

        private void listaTickets()
        {
            dgvTickets.DataSource = admRepositorio.listaTicketsxEstado(dtpTicketDesde.Value,dtpTicketHasta.Value,Convert.ToString(cmbEstado.SelectedValue));

        }



        private void dgvTickets_CellClick(object sender, Telerik.WinControls.UI.GridViewCellEventArgs e)
        {
            if (dgvTickets.Rows.Count > 0)
            {
                if (e.RowIndex != -1)
                {
                    idticket = Convert.ToInt32(e.Row.Cells["resumenidticket"].Value);
                    lblTicketSeleccion.Text = Convert.ToString(e.Row.Cells["ticketnro"].Value);
                    estadosunat = Convert.ToString(e.Row.Cells["estadoticket"].Value);
                }
            }
        }

        private void btnConsultarTicket_Click(object sender, EventArgs e)
        {
            if (idticket !=0)
            {
                if (estadosunat != "0")
                {
                    fact.consultaTicket(lblTicketSeleccion.Text, idticket);
                    lblDocSeleccionado.Text = "No hay ticket seleccionado";
                    idticket = 0;
                }
            }

            listaTickets();
        }

        private void tab3_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
