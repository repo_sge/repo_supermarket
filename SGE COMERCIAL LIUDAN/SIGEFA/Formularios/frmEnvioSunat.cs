﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SIGEFA.Administradores;
using SIGEFA.Entidades;
using SIGEFA.SunatFacElec;
using System.IO;
using DevComponents.DotNetBar;
using System.Threading;
using WinApp.Firmado;
using WinApp.Comun.Dto.Intercambio;
using WinApp.API;


namespace SIGEFA.Formularios
{
    public partial class frmEnvioSunat : Office2007Form
    {

        private string estado = "-1";
        private clsAdmRepositorio clsadmrepo = new clsAdmRepositorio();
        private List<clsRepositorio> lista_repositorio = null;
        private clsEmpresa empresa = null;
        private clsAdmEmpresa admemp = new clsAdmEmpresa();        
        //private DataTable data;

        Facturacion con = new Facturacion();
        

        public frmEnvioSunat()
        {
            InitializeComponent();
        }        

        public void listar_repositorio() 
        {
            try
            {
                lista_repositorio = new List<clsRepositorio>();
                lista_repositorio = clsadmrepo.listar_repositorio(estado, frmLogin.iCodSucursal, frmLogin.iCodAlmacen, dtpFecha.Value);

                if (lista_repositorio != null)
                {
                    if (lista_repositorio.Count > 0)
                    {
                        dg_documentos.Rows.Clear();
                        foreach (clsRepositorio rep in lista_repositorio) 
                        {                            
                            dg_documentos.Rows.Add(rep.Repoid, rep.Tipodoc, rep.Fechaemision, rep.Serie, 
                                                   rep.Correlativo, rep.Monto, rep.Estadosunat, rep.Mensajesunat,
                                                   rep.Nombredoc, rep.Usuario, rep.Fechaemision);
                        }                        
                    }
                    totaldocs.Text = lista_repositorio.Count.ToString();
                }
                else{ dg_documentos.Rows.Clear(); }

                

            }
            catch (Exception e) { MessageBox.Show(e.Message); }
        }

        /*
        public async void Envio() 
        {
            try
            {                
                string rutacertificado = "";
                string tipodocumento = "";
                string _iddocumento = "";
                string[] iddocumento = null;
                bool todocorrecto = false;
                
                if (lista_repositorio != null)
                {
                    if (lista_repositorio.Count > 0)
                    {
                        empresa = admemp.CargaEmpresa3(frmLogin.iCodEmpresa);
                        rutacertificado = @"C:\DOCUMENTOS-" + empresa.Ruc + "\\CERTIFIK\\" + empresa.Certificado;

                        foreach (clsRepositorio r in lista_repositorio)
                        {
                            var tramaXmlSinFirma = Convert.ToBase64String(r.Xml);
                            
                            var firmadoRequest = new FirmadoRequest
                            {
                                TramaXmlSinFirma = tramaXmlSinFirma,
                                CertificadoDigital = Convert.ToBase64String(File.ReadAllBytes(rutacertificado)),
                                PasswordCertificado = empresa.Contrasena,
                                UnSoloNodoExtension = true
                            };                
                           

                            switch (r.Tipodoc)
                            {

                                case 1: tipodocumento = "03"; iddocumento = r.Nombredoc.Split('-'); _iddocumento = iddocumento[2] + "-" + iddocumento[3]; break;//boleta
                                case 2: tipodocumento = "01"; iddocumento = r.Nombredoc.Split('-'); _iddocumento = iddocumento[2] + "-" + iddocumento[3]; break;//factura
                                case 4: tipodocumento = "07"; iddocumento = r.Nombredoc.Split('-'); _iddocumento = iddocumento[2] + "-" + iddocumento[3]; break;//nota credito
                                case 6: tipodocumento = "08"; iddocumento = r.Nombredoc.Split('-'); _iddocumento = iddocumento[2] + "-" + iddocumento[3]; break;//nota debito                            
                            }

                            EnviarDocumentoResponse rpta;

                            await con.Enviar(empresa, _iddocumento, tipodocumento, tramaXmlSinFirma);                            
                            rpta = con.rpta;

                            
                            if (rpta != null)
                            {
                                if (rpta.CodigoRespuesta == "0" && rpta.TramaZipCdr != null)
                                {
                                    r.Estadosunat = "0";
                                    r.Mensajesunat = rpta.MensajeRespuesta;
                                    String ruta = @"C:\DOCUMENTOS-" + empresa.Ruc + "\\CDR\\" + "R-" + r.Nombredoc + ".zip";
                                    File.WriteAllBytes(ruta, Convert.FromBase64String(rpta.TramaZipCdr));

                                    File.WriteAllBytes($"{Program.CarpetaCdr}\\{"R-" + r.Nombredoc + ".zip"}",
                                    Convert.FromBase64String(rpta.TramaZipCdr));

                                     //preguntar si el zip se encuentra en la ruta
									 
                                    if (File.Exists(ruta))
                                    {
                                        r.CDR = File.ReadAllBytes(ruta);
                                    }
                                    else
                                    {
                                        r.CDR = null;
                                        MessageBox.Show("SUNAT no ha devuelto la constancia de recepción\n Por favor verificar archivo en el módulo de\n CONSULTA CDR ");
                                    }
                                    todocorrecto = clsadmrepo.actualiza_repositorio(r);
                                }                                
                                else
                                {

                                    //r.Estadosunat = "-1";
                                    //r.Mensajesunat = rpta.MensajeRespuesta;
                                    //clsadmrepo.actualiza_repositorio(r);
                                    if (rpta.MensajeRespuesta != null)
                                    {
                                        r.Mensajesunat = rpta.MensajeRespuesta;
                                    }
                                    else { r.Mensajesunat = rpta.MensajeError; }


                                    if (rpta.CodigoRespuesta == "1033")
                                    {
                                        r.Estadosunat = "0";
                                        clsadmrepo.actualiza_repositorio(r);

                                        MessageBox.Show("El documento fue registro previamente con otros datos\n Por favor verificar en SUNAT la existencia del mismo\n Doc. a buscar " + _iddocumento);
                                    }
                                    else
                                    {
                                        r.Estadosunat = "-1";
                                        clsadmrepo.actualiza_repositorio(r);
                                        MessageBox.Show("SUNAT no ha devuelto la constancia de recepción\n Por favor verificar archivo en el módulo de\n CONSULTA CDR ");

                                    }
                                }
                            }
                        }

                        if (todocorrecto)
                        {

                            MessageBox.Show("Los documentos fueron enviados de forma correcta");
                            listar_repositorio();
                            //Thread.Sleep(5000);
                            //this.Close();
                        }
                        else
                        {

                            MessageBox.Show("No todos los documentos fueron enviados de forma correcta");
                            listar_repositorio();
                        }
                    }
                }
            }
            catch (Exception a)
            {
                MessageBox.Show(a.Message);
                listar_repositorio();
            }
            finally { Cursor.Current = Cursors.Default; }
        }
        */

        public async void Envio()
        {
            try
            {
                string rutacertificado = "";
                string tipodocumento = "";
                string _iddocumento = "";
                string[] iddocumento = null;
                bool todocorrecto = false;

                if (lista_repositorio != null)
                {
                    if (lista_repositorio.Count > 0)
                    {
                        empresa = admemp.CargaEmpresa3(frmLogin.iCodEmpresa);
                        rutacertificado = @"C:\DOCUMENTOS-" + empresa.Ruc + "\\CERTIFIK\\" + empresa.Certificado;

                        foreach (clsRepositorio r in lista_repositorio)
                        {
                            string ruta1 = "";

                            switch (r.Tipodoc)
                            {

                                case 1:
                                    tipodocumento = "03";
                                    iddocumento = r.Nombredoc.Split('-'); _iddocumento = iddocumento[2] + "-" + iddocumento[3];

                                    ruta1 = Program.CarpetaBoletas + "\\" + r.Nombredoc + ".xml";

                                    break;//boleta

                                case 2:
                                    tipodocumento = "01"; iddocumento = r.Nombredoc.Split('-'); _iddocumento = iddocumento[2] + "-" + iddocumento[3];

                                    ruta1 = Program.CarpetaFacturas + "\\" + r.Nombredoc + ".xml";


                                    break;//factura
                                case 4:
                                    tipodocumento = "07"; iddocumento = r.Nombredoc.Split('-'); _iddocumento = iddocumento[2] + "-" + iddocumento[3];

                                    ruta1 = Program.CarpetaNC + "\\" + r.Nombredoc + ".xml";

                                    break;//nota credito
                                case 6:
                                    tipodocumento = "08"; iddocumento = r.Nombredoc.Split('-'); _iddocumento = iddocumento[2] + "-" + iddocumento[3];

                                    ruta1 = Program.CarpetaND + "\\" + r.Nombredoc + ".xml";

                                    break;//nota debito                            
                            }


                            EnviarDocumentoResponse rpta;
                            if (File.Exists(ruta1))
                            {

                                var tramaXmlSinFirma = Convert.ToBase64String(File.ReadAllBytes(ruta1));
                                var firmadoRequest = new FirmadoRequest
                                {
                                    TramaXmlSinFirma = tramaXmlSinFirma,
                                    CertificadoDigital = Convert.ToBase64String(File.ReadAllBytes(rutacertificado)),
                                    PasswordCertificado = empresa.Contrasena,
                                    UnSoloNodoExtension = true
                                };

                                ICertificador certificador = new Certificador();
                                var respuestaFirmado = await new Firmar(certificador).Post(firmadoRequest);

                                if (!respuestaFirmado.Exito)
                                {
                                    MessageBox.Show(respuestaFirmado.MensajeError);
                                    return;
                                }


                                await con.Enviar(empresa, _iddocumento, tipodocumento, respuestaFirmado.TramaXmlFirmado);
                                rpta = con.rpta;

                            }
                            else
                            {
                                var tramaXmlSinFirma = Convert.ToBase64String(r.Xml);
                                var firmadoRequest = new FirmadoRequest
                                {
                                    TramaXmlSinFirma = tramaXmlSinFirma,
                                    CertificadoDigital = Convert.ToBase64String(File.ReadAllBytes(rutacertificado)),
                                    PasswordCertificado = empresa.Contrasena,
                                    UnSoloNodoExtension = true
                                };
                                await con.Enviar(empresa, _iddocumento, tipodocumento, tramaXmlSinFirma);
                                rpta = con.rpta;

                            }


                            if (rpta != null)
                            {
                                if (rpta.CodigoRespuesta == "0" && rpta.TramaZipCdr != null)
                                {
                                    r.Estadosunat = "0";
                                    r.Mensajesunat = rpta.MensajeRespuesta;
                                    String ruta = @"C:\DOCUMENTOS-" + empresa.Ruc + "\\CDR\\" + "R-" + r.Nombredoc + ".zip";
                                    File.WriteAllBytes(ruta, Convert.FromBase64String(rpta.TramaZipCdr));

                                    File.WriteAllBytes($"{Program.CarpetaCdr}\\{"R-" + r.Nombredoc + ".zip"}",
                                    Convert.FromBase64String(rpta.TramaZipCdr));

                                    /*
									 * preguntar si el zip se encuentra en la ruta
									 */
                                    if (File.Exists(ruta))
                                    {
                                        r.CDR = File.ReadAllBytes(ruta);
                                    }
                                    else
                                    {
                                        r.CDR = null;
                                        MessageBox.Show("SUNAT no ha devuelto la constancia de recepción\n Por favor verificar archivo en el módulo de\n CONSULTA CDR ");
                                    }
                                    todocorrecto = clsadmrepo.actualiza_repositorio(r);
                                }
                                else
                                {

                                    //r.Estadosunat = "-1";
                                    //r.Mensajesunat = rpta.MensajeRespuesta;
                                    //clsadmrepo.actualiza_repositorio(r);
                                    if (rpta.MensajeRespuesta != null)
                                    {
                                        r.Mensajesunat = rpta.MensajeRespuesta;
                                    }
                                    else { r.Mensajesunat = rpta.MensajeError; }


                                    if (rpta.CodigoRespuesta == "1033")
                                    {
                                        r.Estadosunat = "0";
                                        clsadmrepo.actualiza_repositorio(r);

                                        MessageBox.Show("El documento fue registro previamente con otros datos\n Por favor verificar en SUNAT la existencia del mismo\n Doc. a buscar " + _iddocumento);
                                    }
                                    else
                                    {
                                        r.Estadosunat = "-1";
                                        clsadmrepo.actualiza_repositorio(r);
                                        MessageBox.Show("SUNAT no ha devuelto la constancia de recepción\n Por favor verificar archivo en el módulo de\n CONSULTA CDR ");

                                    }
                                }
                            }
                        }

                        if (todocorrecto)
                        {

                            MessageBox.Show("Los documentos fueron enviados de forma correcta");
                            listar_repositorio();
                            //Thread.Sleep(5000);
                            //this.Close();
                        }
                        else
                        {

                            MessageBox.Show("No todos los documentos fueron enviados de forma correcta");
                            listar_repositorio();
                        }
                    }
                }
            }
            catch (Exception a)
            {
                MessageBox.Show(a.Message);
                listar_repositorio();
            }
            finally { Cursor.Current = Cursors.Default; }
        }

        private void btn_envio_Click(object sender, EventArgs e)
        {
            this.Envio();
        }
       
        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmEnvioSunat_Load(object sender, EventArgs e)
        {
            cb_estado.SelectedIndex = 0; 
            //listar_repositorio();            
        }

        private void cb_estado_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cb_estado.SelectedIndex == 0)
            {
                btn_envio.Enabled = true;
                estado = "-1";
                listar_repositorio();
            }
            else
            {
                btn_envio.Enabled = false;
                estado = "0";
                listar_repositorio_Enviados();
            }       
            
        }

        private void listar_repositorio_Enviados()
        {
            try
            {
                

                lista_repositorio = new List<clsRepositorio>();
                lista_repositorio = clsadmrepo.listar_repositorio_Enviados(estado, frmLogin.iCodSucursal, frmLogin.iCodAlmacen, dtpFecha.Value);

                if (lista_repositorio != null)
                {
                    if (lista_repositorio.Count > 0)
                    {
                        dg_documentos.Rows.Clear();
                        foreach (clsRepositorio rep in lista_repositorio)
                        {
                            dg_documentos.Rows.Add(rep.Repoid, rep.Tipodoc, rep.Fechaemision, rep.Serie,
                                                   rep.Correlativo, rep.Monto, rep.Estadosunat, rep.Mensajesunat,
                                                   rep.Nombredoc, rep.Usuario, rep.Fechaemision);
                        }
                    }
                    totaldocs.Text = lista_repositorio.Count.ToString();
                }
                else { dg_documentos.Rows.Clear(); }
            }
            catch (Exception e) { MessageBox.Show(e.Message); }
        }

        private void frmEnvioSunat_Shown(object sender, EventArgs e)
        {
            if (lista_repositorio != null)
            {
                if (lista_repositorio.Count > 0)
                {
                    //Thread.Sleep(5000);
                    //this.Envio();
                }
            }
            //else { this.Close(); }
        }

    }
}
